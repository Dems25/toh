<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        return view('user_pages.home'); 
    }
    
    public function about()
    {
        return view('user_pages.about');
    }

    public function academicProduct()
    {
      return view('user_pages.acadprod');
    }

    public function chat()
    { 
      return view('user_pages.chat');
    }

    public function contact()
    {
       return view('user_pages.contact');
    }

    public function FAQ()
    {
      return view('user_pages.FAQ');
    }

    public function area()
    {
      return view('user_pages.area');
    }
  
    public function cart()
    {
      return view('user_pages.cart');
    }

    public function profile()
    { 
      return view('user_pages.profile');
    }

    public function academic_support()
    {
      return view('user_pages.acadsupport');
    }

    public function onlineclass_product()
    { 
      return view('user_pages.ocprod');
    }
  
    public function onlineclass_form()
    { 
      return view('user_page_onlineclass_form');
    }

    public function onlineclass()
    {
      return view('user_page.onlineclass');
    }
  
    public function vocational_product()
    {
      return view('user_page.vocprod');
    }

    public function vocational_training()
    {
      return view('user_page.voctraining');
    }
 
}

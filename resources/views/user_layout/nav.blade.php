<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <title>Home</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Nucleo Icons -->
  <link href="assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <link href="{{url('assets/css/font-awesome.css')}}" rel="stylesheet" />
  <link href="{{url('assets/css/mystyle.css')}}" rel="stylesheet" />
  <link href="{{url('assets/css/nucleo-svg.css')}}" rel="stylesheet" />
  <!-- CSS Files-->
  <link href="{{url('assets/css/argon-design-system.css?v=1.2.0')}}" rel="stylesheet" />
</head>
<body class="landing-page">
    <!-- Navbar -->
 <nav id="navbar-main" class=" navbar navbar-expand-md navbar-main navbar-dark bg-primary">
    <div class="container">
      <a class="navbar-brand mr-lg-5">
         <img src="{{url('assets/img/own/teacheron.png')}}">

      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbar_global">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="../../../index.html">
                <img src="{{url('assets/img/own/teacheron.png')}}">
                
                
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <ul class="navbar-nav align-items-lg-center ml-lg-auto" >
       
                <li class="nav-item">
                  <a class="nav-link nav-link-icon" href="{{url('/about')}}">
                    <!-- <i class="ni ni-favourite-28"></i>
                    <span class="nav-link-inner--text d-lg-none">Favorites</span> -->
                    About Us
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-link-icon" href="{{url('/chat')}}">
                   
                    Chat With Us
                  </a>
                </li>
                   <li class="nav-item">
                  <a class="nav-link nav-link-icon" href="{{url('/contact')}}">
                    
                    Contact Us
                  </a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link nav-link-icon" href="{{url('/home')}}">
                    
                    Platform Services
                  </a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link nav-link-icon" href="{{url('/FAQ')}}">
                    
                    FAQ
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link nav-link-icon" href="{{url('/area')}}">
                   
                    My Area
                  </a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link nav-link-icon" href="{{url('/cart')}}">
                
                    Cart
                  </a>
                </li>
          <li class="nav-item d-none d-lg-block ml-lg-4">
            <li class="nav-item dropdown">
                  <a class="nav-link nav-link-icon" href="#" id="navbar-success_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <!-- <img src="assets/img/faces/team-4.jpg" class="rounded-circle"> --> 
                   
                      <i class="ni ni-circle-08 mr-1" style="font-size: 20px;"> {{ Auth::user()->name }}</i>
              
                   
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-success_dropdown_1">
                    <a class="dropdown-item" href="{{url('/profile')}}">My Account</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                    </a>
                  </div>
              </li>
          </li>
        </ul>
      </div>
    </div>
  </nav>
    <div class="container">
       @yield('content')
    </div>
  </body>
 
  <footer class="footer">
      <div class="container">
        <div class="row row-grid align-items-center mb-5">
          <div class="col-lg-6">
            <h3 class="text-primary font-weight-light mb-2">Thank you for supporting us!</h3>
            <h4 class="mb-0 font-weight-light">Let's get in touch on any of these platforms.</h4>
          </div>
          <div class="col-lg-6 text-lg-center btn-wrapper">
            <button target="_blank" href="" rel="nofollow" class="btn btn-icon-only btn-twitter rounded-circle">
              <span class="btn-inner--icon"><i class="fa fa-twitter"></i></span>
            </button>
            <button target="_blank" href="" rel="nofollow" class="btn-icon-only rounded-circle btn btn-facebook">
              <span class="btn-inner--icon"><i class="fa fa-facebook"></i></span>
            </button>
            <button target="_blank" href="" rel="nofollow" class="btn btn-icon-only btn-whatsapp rounded-circle">
              <span class="btn-inner--icon"><i class="fa fa-whatsapp"></i></span>
            </button>
            <button target="_blank" href="" rel="nofollow" class="btn btn-icon-only btn-instagram rounded-circle">
              <span class="btn-inner--icon"><i class="fa fa-instagram"></i></span>
            </button>
          </div>
        </div>
        <hr>
        <div class="row align-items-center justify-content-md-between">
          <div class="col-md-6">
            <div class="copyright">
              &copy; 2020 <a href="" target="_blank">Teachers On Hire</a>.
            </div>
          </div>
        </div>
      </div>
</footer>

  </html>

  <!--   Core JS Files   -->
  <script src="{{url('assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{url('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{url('assets/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
  <script src="{{url('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  
  <!-- @if (session('status')) -->
  <!-- @endif -->

@extends('user_layout.nav')
@section('content')
          <div class="wrapper m-5">
            <h2 class="title text-lg-center">Frequently asked Questions (FAQ)</h2>
            <div class="row">
              <div class="col-sm-2"></div>
              <div class="col-sm-8">
              <div class="form-group">
                <div class="input-group mb-4">
                    <div class="input-group-prepend">
                     <span class="input-group-text"><i class="fa fa-search"></i></span>
                    </div>
                    <input class="form-control" placeholder="Search for Answer" type="text">
                </div>
                </div>
              </div>
              </div><!--eND OF ROW-->

               <div class="card shadow mb-5">
                <div class="card-body bg-warning "style="color: white">
                  <div class="row">
                    <div class="col-sm-2 ">
                      <h1 class="text-white">Q:</h1>
                    </div>
                    <div class="col-sm-10">
                      <h5 class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h5>
                    </div>
                   </div>
                   <hr style="height:2px;border-width:0;color:gray;background-color:white">
                   <div class="row">
                       <div class="col-sm-2">
                      <h1 class="text-white">A:</h1>
                    </div>
                    <div class="col-sm-10 ">
                      <h6 class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus.</h6>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card shadow">
                <div class="card-body bg-warning "style="color: white">
                  <div class="row">
                    <div class="col-sm-2 ">
                      <h1 class="text-white">Q:</h1>
                    </div>
                    <div class="col-sm-10">
                      <h5 class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h5>
                    </div>
                   </div>
                   <hr style="height:2px;border-width:0;color:gray;background-color:white">
                   <div class="row">
                       <div class="col-sm-2">
                      <h1 class="text-white">A:</h1>
                    </div>
                    <div class="col-sm-10 ">
                      <h6 class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus.</h6>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

@endsection

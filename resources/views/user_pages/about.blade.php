
@extends('user_layout.nav')
@section('content')
          <div class="wrapper m-5">
              <h5 class="title mb-3">Statistical Report</h5>
              <div class="row  mb-5">
                <div class="col-sm-1"><span></span></div>
                <div class="col-sm-3 text-lg-center text-white">
                    <div class="card bg-warning p-3" style="font-size: 20px; color: white;">
                      <i class="fa fa-book "></i>
                      <h4>Course</h4>
                      <hr>
                      <h3 style="font-size: 30px;color: white;">12</h3>
                    </div>
                </div>
                <div class="col-sm-3 text-lg-center text-white">
                    <div class="card bg-primary p-3" style="font-size: 20px; color: white;">
                      <i class="fa fa-user "></i>
                      <h4>Teacher</h4>
                      <hr>
                      <h3 style="font-size: 30px; color: white">8</h3>
                    </div>
                </div>
                <div class="col-sm-3 text-lg-center ">
                    <div class="card bg-success  p-3" style="font-size: 20px; color: white;">
                      <i class="fa fa-users "></i>
                      <h4>Student</h4>
                      <hr>
                      <h3 style="font-size: 30px;color: white">34</h3>
                    </div>
                </div>
                <div class="col-sm-1"><span></span></div>
                
              </div>
              <div class="row align-items-center">
                <div class="col-lg-8 mb-5">
                  <div class="info info-horizontal info-hover-primary">
                    <div class="description pl-4">
                      <h5 class="title">Mission</h5>
                      <p>We Connect Professional Educators to Dedicated Learners.</p>
                      <a href="#" class="text-info">Learn more</a>
                    </div>
                  </div>
                </div>
              </div>  

              <div class="row align-items-center">
                <div class="col-lg-6">
                  <div class="info info-horizontal info-hover-primary">
                    <div class="description pl-4">
                      <h5 class="title">Why Us</h5>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      <a href="#" class="text-info">Learn more</a>
                    </div>
                  </div>
                </div>
              </div>  

           
                
      
          </div>

@endsection






@extends('user_layout.nav')
@section('content')
          
<div class="wrapper m-5">
<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Client Statistics</th>
            <th>Contract</th>
            <th>Course Taught</th>
            <th class="text-right">Actions</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td>odio euismod</td>
            <td>2012</td>
            <td>maecenas volutpat blandit aliquam etiam</td>
            <td class="td-actions text-right">
              <button type="button" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-circle-08 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-settings-gear-65 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-danger btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-fat-remove pt-1"></i>
                </button>
            </td>
        </tr>
                <tr>
            <td class="text-center">1</td>
            <td>odio euismod</td>
            <td>2012</td>
            <td>maecenas volutpat blandit aliquam etiam</td>
            <td class="td-actions text-right">
              <button type="button" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-circle-08 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-settings-gear-65 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-danger btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-fat-remove pt-1"></i>
                </button>
            </td>
        </tr>
                <tr>
            <td class="text-center">1</td>
            <td>odio euismod</td>
            <td>2012</td>
            <td>maecenas volutpat blandit aliquam etiam</td>
            <td class="td-actions text-right">
              <button type="button" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-circle-08 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-settings-gear-65 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-danger btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-fat-remove pt-1"></i>
                </button>
            </td>
        </tr>

    </tbody>
</table>
          </div>

          
<div class="container-fluid">
  <h1>The Client Area for quick access of products which include: (1) For Learners: Teachers direct contacts - Courses Details and
Quick Access - Offers, (2) For Teachers: Clients direct contacts – Statistics of Clients, Contracts, and Courses taught</h1>
</div>


@endsection


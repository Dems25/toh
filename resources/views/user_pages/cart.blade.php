
@extends('user_layout.nav')
@section('content')
          <div class="wrapper m-7">
            <b> <h2 class="title  mb-2 ">MY CART</h2></b>
           <table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Course Name</th>
            <th>Price</th>
            <th class="text-right">Actions</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td> <img src="assets/img/faces/team-1.jpg" alt="Rounded image" class="img-fluid rounded shadow-lg" style="width: 50px;"> Maecenas volutpat blandit aliquam etiam</td>
            <td>$ 29.00 </td>
            <td class="td-actions text-right">
              <button type="button" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-circle-08 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-settings-gear-65 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-danger btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-fat-remove pt-1"></i>
                </button>
            </td>
        </tr>
              <tr>
            <td class="text-center">2</td>
            <td> <img src="assets/img/faces/team-1.jpg" alt="Rounded image" class="img-fluid rounded shadow-lg" style="width: 50px;"> Maecenas volutpat blandit aliquam etiam</td>
            <td>$ 21.00 </td>
            <td class="td-actions text-right">
              <button type="button" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-circle-08 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-settings-gear-65 pt-1"></i>
              </button>
              <button type="button" rel="tooltip" class="btn btn-danger btn-icon btn-sm " data-original-title="" title="">
                <i class="ni ni-fat-remove pt-1"></i>
                </button>
            </td>
        </tr>
        <tr class="bg-info">
          <td colspan="3">
            <b>Total </b>
          </td>
          <td class="text-right">
            <b> $ 50.00</b>
          </td>
        </tr>

    </tbody>
</table>

 <a href="" class="btn btn-warning float-right ">Check Out</a>

          </div>
<br><br><br>
@endsection

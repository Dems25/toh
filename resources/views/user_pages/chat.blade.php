
@extends('user_layout.nav')
@section('content')
          <div class="wrapper m-5">
        
            <div class="row">
              <div class="container-fluid">
    <h1> Message Us Now! </h1>
    <h3>Online direct chatting with one of our staff</h3>
    <div class="jumbotron">
      <div class="row mb-5">
        <div class="col-sm-3">
          <div class="bg-success card p-2">
            <span>
            <img src="assets/img/faces/team-1.jpg" alt="Rounded image" class="img-fluid rounded-circle shadow-lg" style="width: 30px;"><small class="ml-2 text-white">Mark</small></span>
            <small class="ml-7" style="font-size: 30px">Hi </small>
          </div>
        </div>
        <div class="col-sm-8">
          
        </div>
        <div class="col-sm-8">
          
        </div>
        <div class="col-sm-3 ">
           <div class="bg-info card p-2">
            <span>
            <img src="assets/img/faces/team-2.jpg" alt="Rounded image" class="img-fluid rounded-circle shadow-lg" style="width: 30px;"><small class="ml-2 text-white">Arya</small></span>
            <small class="ml-7" style="font-size: 30px">Hello </small>
          </div>
        </div>
      </div>
      <div class="form-group">
                <div class="input-group input-group-alternative mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-comment"></i></span>
                  </div>
                  <input class="form-control" placeholder="Type a Message ..." type="text">
                </div>
              </div>
      <a href="" class="btn btn-primary btn-icon float-right">Send</a>
</div>
                
            </div>
          </div>
  </div>



@endsection


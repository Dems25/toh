
@extends('user_layout.nav')
@section('content')
<br><br>
<div>
    <h1 class="display-1">Contact Us Now</h1>
    <h2 class="display-4 font-weight-normal text-white">The time is right now!</h2>
</div>

<div>
     <h1 class="display-3">HOW TO FIND US</h1>
     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
     <h3 class="display-4 mb-4">EMAIL:  TeachersOnHire@gmail.com </h3>
     <h3 class="display-4">CONTACT:   (02) 1234- 4567</h3>
</div>
                      
<div>
    <a href="#"><span class="btn-inner--icon ml-2"><i class="fa fa-twitter-square" style="color: #55ACEE; font-size: 50px"></i></span></a> 
    <a href="#"><span class="btn-inner--icon ml-2"><i class="fa fa-facebook-square" style="color: #3B5998; font-size: 50px"></i></span></a>
    <a href="#"> <span class="btn-inner--icon ml-2"><i class="fa fa-instagram" style="color: #405DE6; font-size: 50px"></i></span></a>
    <a href="#"> <span class="btn-inner--icon ml-2"><i class="fa fa-whatsapp" style="color: #EA4335; font-size: 50px"></i></span></a>
</div>
<br><br><br><br><br>
@endsection



@extends('user_layout.nav')

@section('content')
          <div class="wrapper m-5">
            <h4 class="title mb-5">SERVICES</h4>
            <div class="row">
             
              <div class="col-md-4"><a href="acadsupport.html">
                <div class="card">
                    <img class="card-img-top" src="assets/img/own/teacher1.jpg" alt="Card image">
                    <div class="card">
                      <h3 class="card text-center">Academic Support</h3>
                      <a href="acadsupport.html" class="btn btn-primary" style="width:100%;" >View Class</a>
                    </div>
                  </div>
              </div>
                  <div class="col-md-4"><a href="onlineclass.html">
                <div class="card">
                    <img class="card-img-top" src="assets/img/own/teacher2.jpg" alt="Card image">
                    <div class="card">
                      <h3 class="card text-center">Online Class</h3>
                      <a href="onlineclass.html" class="btn btn-primary" style="float: right">View Class</a>
                    </div>
                  </div>
                </a>
              </div>  <div class="col-md-4"><a href="voctraining.htm">
                <div class="card">
                    <img class="card-img-top" src="assets/img/own/teacher3.jpg" alt="Card image">
                    <div class="card">
                      <h3 class="card text-center" >Vocational Training</h3>
                      <a href="voctraining.html" class="btn btn-primary" style="float: right">View Class</a>
                    </div>
                  </div>
                </a>
              </div>
                
            </div>
          </div>
<br><br><br>
@endsection

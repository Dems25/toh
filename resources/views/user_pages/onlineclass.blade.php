
@extends('user_layout.nav')
@section('content')
          <div class="wrapper m-5">
            <h4 class="title mb-5">ONLINE CLASS</h4>
            <div class="row">

              <div class="col-md-12 mb-5 ">
                  <div class="card shadow">
                <div class="card bg-danger">
                  <h1 class="card-title m-5">ENGLISH</h1>
                  <div class="col-sm-12">
                  <button class="btn btn-primary col-sm-3 float-right mb-2"  data-toggle="collapse" data-target="#demo">View Subject</button></div>
                    <div id="demo" class="collapse">
                <div class="nav-wrapper m-5">
                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-user-circle-o mr-2"></i>KG-Grade5</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 6 - Grade -9</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 10 - Grade -12</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-user mr-2"></i>College / University</a>
                  </li>
                </ul>
              </div>

              <div class="tab-content m-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab col-sm-8">
                      <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>




                  </div>
                </div>
              </div>
            </div>
            
            </div>

                          <div class="col-md-12 mb-5 ">
                  <div class="card shadow">
                <div class="card bg-info">
                  <h1 class="card-title m-5">MATHEMATIC</h1>
                  <div class="col-sm-12">
                  <button class="btn btn-primary col-sm-3 float-right mb-2"  data-toggle="collapse" data-target="#demo2">View Subject</button></div>
                    <div id="demo2" class="collapse">
                <div class="nav-wrapper m-5">
                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-user-circle-o mr-2"></i>KG-Grade5</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 6 - Grade -9</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 10 - Grade -12</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-user mr-2"></i>College / University</a>
                  </li>
                </ul>
              </div>

              <div class="tab-content m-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab col-sm-8">
                      <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>




                  </div>
                </div>
              </div>
            </div>
            
            </div>
             
                           <div class="col-md-12 mb-5 ">
                  <div class="card shadow">
                <div class="card bg-warning">
                  <h1 class="card-title m-5">SCIENCE</h1>
                  <div class="col-sm-12">
                  <button class="btn btn-primary col-sm-3 float-right mb-2"  data-toggle="collapse" data-target="#demo3">View Subject</button></div>
                    <div id="demo3" class="collapse">
                <div class="nav-wrapper m-5">
                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-user-circle-o mr-2"></i>KG-Grade5</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 6 - Grade -9</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 10 - Grade -12</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-user mr-2"></i>College / University</a>
                  </li>
                </ul>
              </div>

              <div class="tab-content m-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab col-sm-8">
                      <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>




                  </div>
                </div>
              </div>
            </div>
            
            </div>
             
                           <div class="col-md-12 mb-5 ">
                  <div class="card shadow">
                <div class="card bg-default">
                  <h1 class="card-title m-5">IT</h1>
                  <div class="col-sm-12">
                  <button class="btn btn-primary col-sm-3 float-right mb-2"  data-toggle="collapse" data-target="#demo4">View Subject</button></div>
                    <div id="demo4" class="collapse">
                <div class="nav-wrapper m-5">
                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-user-circle-o mr-2"></i>KG-Grade5</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 6 - Grade -9</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 10 - Grade -12</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-user mr-2"></i>College / University</a>
                  </li>
                </ul>
              </div>

              <div class="tab-content m-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab col-sm-8">
                      <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>




                  </div>
                </div>
              </div>
            </div>
            
            </div>


                          <div class="col-md-12 mb-5 ">
                  <div class="card shadow">
                <div class="card bg-info">
                  <h1 class="card-title m-5">MUSIC</h1>
                  <div class="col-sm-12">
                  <button class="btn btn-primary col-sm-3 float-right mb-2"  data-toggle="collapse" data-target="#demo5">View Subject</button></div>
                    <div id="demo5" class="collapse">
                <div class="nav-wrapper m-5">
                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-user-circle-o mr-2"></i>KG-Grade5</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 6 - Grade -9</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 10 - Grade -12</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-user mr-2"></i>College / University</a>
                  </li>
                </ul>
              </div>

              <div class="tab-content m-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab col-sm-8">
                      <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>




                  </div>
                </div>
              </div>
            </div>
            
            </div>
             


                <div class="col-md-12 mb-5 ">
                  <div class="card shadow">
                <div class="card bg-success">
                  <h1 class="card-title m-5">ART</h1>
                  <div class="col-sm-12">
                  <button class="btn btn-primary col-sm-3 float-right mb-2"  data-toggle="collapse" data-target="#demo6">View Subject</button></div>
                    <div id="demo6" class="collapse">
                <div class="nav-wrapper m-5">
                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-user-circle-o mr-2"></i>KG-Grade5</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 6 - Grade -9</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="fa fa-user mr-2"></i>Grade 10 - Grade -12</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fa fa-user mr-2"></i>College / University</a>
                  </li>
                </ul>
              </div>

              <div class="tab-content m-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab col-sm-8">
                      <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade " id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                       <table class="table">
                        <tr>
                          <td>
                            #1
                          </td>
                          <td colspan="5">
                            Reading Phonics
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #2
                          </td>
                          <td colspan="5">
                            Reading Comprehension
                          </td>
                          <td>
                           <a href="ocprod.html"> <button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            #3
                          </td>
                          <td colspan="5">
                            Reading Difficulties
                          </td>
                          <td>
                            <a href="ocprod.html"><button class="btn btn-primary float-right"> JOIN NOW</button></a>
                          </td>
                        </tr>
                      </table>
                    </div>




                  </div>
                </div>
              </div>
            </div>
            
            </div>
             
             
                
            </div>
          </div>

         
@endsection
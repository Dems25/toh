
@extends('user_layout.nav')
@section('content')
<div class="container mt-3 mb-5 ">
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div class="card bg-secondary shadow border-0">
            <div class="card-header bg-white">
              <h3 class="display-4">Class Registration For:</h3>
              <p>English: Grade 5: Reading Comprehension </p>
       
            </div>
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
              </div>
              <form role="form">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                      </div>
                      <input class="form-control" placeholder="Name" type="email">
                    </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                      </div>
                      <input class="form-control" placeholder="Nationality" type="email">
                    </div>
                    </div>
                  </div>
                    <div class="col-sm-6">
                    <div class="form-group mb-3">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                      </div>
                      <input class="form-control" placeholder="Contact Number" type="number">
                    </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                  </div>
                  <input class="form-control datepicker" placeholder="Select date" type="text" value="06/20/2020" />
                </div>
              </div>
          
                  </div>
  
             
                
                  
                </div>
       
                <div class="text-center ">
                  <a href="cart.html" class=""><button type="button" class="btn btn-primary my-4 float-right" ><i class="fa fa-shopping-cart mr-2"></i>ADD TO CART</button></a>
                </div>
              </form>
            </div>
          </div>
     
        </div>
      </div>
    </div>

           
@endsection
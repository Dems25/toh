<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'Auth\LoginController@login');

Route::get('/about', 'HomeController@about')->name('about');

Route::get('/academicProduct', 'HomeController@academicProduct')->name('academicProduct');

Route::get('/chat', 'HomeController@chat')->name('chat');

Route::get('/contact', 'HomeController@contact')->name('contact');

Route::get('/FAQ', 'HomeController@FAQ')->name('FAQ');

Route::get('/area', 'HomeController@area')->name('area');

Route::get('/cart', 'HomeController@cart')->name('cart');

Route::get('/profile', 'HomeController@profile')->name('profile');

Route::get('/academic_support', 'HomeController@academic_support')->name('academic_support');

Route::get('/onlineclass_product', 'HomeController@onlineclass_product')->name('onlineclass_product');

Route::get('/onlineclass_form', 'HomeController@onlineclass_form')->name('onlineclass_form');

Route::get('/onlineclass', 'HomeController@onlineclass')->name('onlineclass');

Route::get('/vocational_product', 'HomeController@vocational_product')->name('vocational_product');

Route::get('/vocational_training', 'HomeController@vocational_training')->name('vocational_training');

Route::get('/home', 'HomeController@index')->name('home');


